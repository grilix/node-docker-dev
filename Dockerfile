FROM node:12

VOLUME /node_modules
RUN echo "--modules-folder /node_modules" | tee -a ~/.yarnrc > /dev/null
ENV PATH $PATH:/node_modules/.bin

VOLUME /usr/src/app
WORKDIR /usr/src/app
